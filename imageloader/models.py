from django.db import models
from datetime import datetime
import random
import os
from PIL import Image

dir_path = os.path.dirname(os.path.realpath(__file__))
file_path = "{dir_path}/static/images/".format(dir_path=dir_path)
thumb_path = "{dir_path}/static/thumbs/".format(dir_path=dir_path)

thumb_width = 120
thumb_height = 120


class LoadedImage(models.Model):
    title = models.CharField(max_length=64)
    filename = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)

    def to_dict(self):
        return {
            "id": self.id,
            "title": self.title,
            "filename": self.filename,
            "created_at": str(self.created_at)
        }


def add_image(img_file, title):
    try:
        # generate random filename hash
        filename = "{0}.{1}".format(random.getrandbits(128), img_file.name.split(".")[-1])

        # save image to folder
        with open(file_path + filename, 'wb+') as f:
            for chunk in img_file.chunks():
                f.write(chunk)

        # create the thumbnail
        original = Image.open(file_path + filename)
        thumbnail = original.resize((thumb_width, thumb_height))
        thumbnail.save(thumb_path + filename)

        # add to database
        img = create_image(filename, title)
    except:
        return None
    else:
        return img


def create_image(filename, title):
    img = LoadedImage()
    img.title = title
    img.filename = filename
    img.created_at = datetime.now()
    img.save()
    return img


def delete_image(img_id):
    try:
        img = LoadedImage.objects.get(id=img_id)
        filename = img.filename
        img.delete()
        os.remove(file_path + filename)
        os.remove(thumb_path + filename)
    except:
        return False
    else:
        return True
