from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='imageloader_list'),
    path('upload/', views.form, name="imageloader_form"),
    path('delete/<int:img_id>/', views.delete, name="imageloader_delete"),
    path('view/<int:img_id>/', views.view, name="imageloader_view")
]
