from django import forms


class UpdateForm(forms.Form):
    title = forms.CharField(max_length=64)


class CreateForm(UpdateForm):
    image_file = forms.ImageField()
