from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.http import QueryDict
from .models import LoadedImage, add_image, delete_image
from .forms import CreateForm, UpdateForm


def index(request):
    return render(request, "imageloader/list.html", {
        'loaded_images': LoadedImage.objects.all()
    })


def view(request, img_id):
    return render(request, "imageloader/view.html", {
        'image': LoadedImage.objects.get(id=img_id)
    })


def form(request):
    if request.method == 'POST':

        create_form = CreateForm(request.POST, request.FILES)

        if create_form.is_valid():
            if add_image(request.FILES['image_file'], create_form.cleaned_data["title"]) is not None:
                return redirect('imageloader_list')

    return render(request, "imageloader/form.html", {})


def delete(request, img_id):
    if not delete_image(img_id):
        raise Http404("image not found")
    return redirect('imageloader_list')


def api_response(func):
    def wrapper(*args, **kwargs):
        status, result = func(*args, **kwargs)
        return JsonResponse({"result": result}, status=200 if status else 404)
    return wrapper


@csrf_exempt
@api_response
def api(request):
    if request.method == 'GET':
        # get list of all images
        return True, [img.to_dict() for img in LoadedImage.objects.all()]

    elif request.method == 'POST':
        # create new image
        create_form = CreateForm(request.POST, request.FILES)
        if create_form.is_valid():
            img = add_image(request.FILES['image_file'], create_form.cleaned_data["title"])
            if img is None:
                return False, "failed to create image"
            else:
                return True, img.to_dict()
        else:
            return False, "invalid data submitted"

    return False, "method not implemented"


@csrf_exempt
@api_response
def api_param(request, img_id):
    if request.method == 'GET':
        # get image by id
        try:
            return True, LoadedImage.objects.get(id=img_id).to_dict()
        except:
            return False, "image not found"

    elif request.method == 'PUT':
        # update existing image
        data = QueryDict(request.body)
        update_form = UpdateForm(data)

        if update_form.is_valid():
            try:
                img = LoadedImage.objects.get(id=img_id)
                img.title = update_form.cleaned_data["title"]
                img.save()
            except:
                return False, "update image failed"
            else:
                return True, img.to_dict()
        else:
            return False, "invalid data submitted"

    elif request.method == 'DELETE':
        # delte existing image
        if delete_image(img_id):
            return True, "image deleted"
        else:
            return False, "image not found"

    return False, "method not implemented"
