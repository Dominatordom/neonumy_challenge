from django.urls import path

from . import views

urlpatterns = [
    path('', views.api, name='imageloader_api'),
    path('<int:img_id>/', views.api_param, name='imageloader_api_param'),
]
