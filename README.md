# README #

This README documents what steps are necessary to get your application up and running.

### Docker ###

To create a docker container and for this project simply run:
	
	docker-compose up -d

The application is available by default on localhost:8000

Note: a database container is automatically created and added to the docker network, no other steps should be necessary

### Local Development ###

To run the application locally do the following:

* create a database for this project
* change neonumy/settings.py DATABASES section
* run python3 manage.py migrate
* run python3 manage.py runserver

The application is available by default on localhost:8000

### Available Routes ###

The base url ("/") is used to list, add and delete images (as requested)

The api returns data in json format and has the following routes:

* GET api/ -> get list of all images
* POST api/ -> create a new image, returns the created image
* GET api/<id> -> get an image by its id
* UPDATE api/<id> -> change an images details (not the image itself, personal choice to make things different from create)
* DELETE api/<id> -> delete an image

If anything fails 404 is returned, more information about the error can be found in the json under the "result"

### Objectives ###

Completed:
* Create a page to list all uploaded images with thumbnails.
* Each image should have a button to go to details where it shows the image in its real
size
* Create a button to delete the image
* Create a page to upload new images
* Create and API that covers all CRUD actions
* Add a readme file with instructions on how to run the project for development.
* Use Postgres as your main database
* Setup the project in Docker
* Setup docker-compose

Not-Completed:
* Make the API in GraphQL
* Create some unit test for your logic

### Final Notes ###

Due to a lack of time on my behalf (personal issues) I did not complete all the objectives, knowing I had limited time I made the decision to completely skip any design elements, API with GraphQL and creating unit tests.

Given more time I would have implemented a simple bootstrap based template for the pages, changed some functionalities to work with ajax instead of page jumping and redirecting.

Also the API was created almost from scratch where I could have used django-restfull but since I felt comfortable making my own and never having used django-restfull or even created a Django application with an API (all my experience with APIs have been in CherryPy and Flask) I decided to save time and just create it this way.

I also would have liked to look into GraphQL and Django unit tests, two things I have no experience with but this will have to be during my free time.

Hopefully you enjoy my project.

Dominic Santos